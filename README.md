# Semesteroppgave 1

Denne oppgaven har vi allerede fork'et for deg. Du kan finne din kopi her: [https://git.app.uib.no/ii/inf101/22v/assignments/](https://git.app.uib.no/ii/inf101/22v/assignments/).

Det er noen få studenter vi ikke har klart å få tak i riktig brukernavn til. Dersom du ikke finner din kopi av semesteroppgave 1 på linken over, gi beskjed til Sondre Bolland så fort som mulig.

Her finner du:
- [Ekstra tips](#ekstra-tips)
- [Errata](#errata) (feil i guiden og boilerplate-kode)


# Ekstra tips

Vi samler her tips og hint som vi oppdager underveis har verdi for flere.

Et UML-diagram av Torstein sin eksempel-løsning: [tetris-uml.pdf](./tetris-uml.pdf). Merk at (som klassediagrammer flest) er det mye som er utelatt, men et subjektivt utvalg av de viktigste feltvariablene, metodene og sammenhengene er inkludert. Det er forsøkt at utgående piler til høyre er "har" -forhold ("A har en B"), mens utgående piler oppover er et "er" -forhold via arv eller implementasjon av grensesnitt ("A er en B").


### Steg 1 (grid)

- Tips: ikke benytt en native array (f. eks. `T[][]`) for å representere grid'et. Generics og native array samtidig vil generere warnings, og du mister typesikkerhet. I stedet bør du benytte en List, for eksempel `ArrayList<ArrayList<T>>` (eller `ArrayList<T>` dersom du tar utgangspunkt i lab 5).

- Tips: en klasse kan ha to konstruktører, så lenge signaturene (altså hvilke typer parametrene har) er ulike. Det er mulig å kalle én konstruktør fra en annen. For eksempel, en klasse `AClass` kan ha en konstruktør `AClass(int, boolean)` og en annen konstruktør `AClass(int)`. Den sistnevnte konstruktøren kan for eksempel implementeres slik:

```java
AClass(int x) {
    this(x, false);
}
```

I eksempelet gjør den andre konstruktøren et kall til den første konstruktøren med en "standard" verdi (i.e. `false`).

### Steg 2 (tegne brettet)

- `TetrisBoard` skal utvide klassen `Grid<Tile>`. Konstruktører blir ikke automatisk arvet på samme måte som feltvariabler og metoder, så det vil være behov for en konstruktør i TetrisBoard. Denne bør kalle på konstruktøren til Grid, ved å kalle metoden `super`. For eksempel:

```java
TetrisBoard(int rows, int cols) {
        super(rows, cols, null);
}
```

Dette vil gjøre at konstruktøren til superklassen (altså Grid) blir utført.

- I guiden står det at vi skal definere en metode i TertrisViewable som returnerer en `Iterable<CoordinateItem<Tile>>`. Merk at en *Iterable* ikke er det samme som en *Iterator*. Guiden spesifiserer ikke hva metoden skal hete, men kall den gjerne noe som beskriver *hva* som itereres over. I Torstein sitt løsningsforslag (se [uml](./tetris-uml.pdf)) kaller han den `tilesInBoard`.

- Et objekt av typen `TetrisBoard` har flere typer. For eksempel har det typen `TetrisBoard`, slik at vi kan si `TetrisBoard tetrisBoard = new TetrisBoard(15, 10);`. Men fordi TetrisBoard utvider Grid&lt;Tile&gt;, har objektet også typen `Grid<Tile>`. Vi kan med andre ord si `Grid<Tile> gridOfTiles = tetrisBoard;` uten at noe blir feil. Grid&lt;Tile&gt; implementerer IGrid&lt;Tile&gt; -- derfor har objekter i klassen TetrisBoard også typen `IGrid<Tile>`. IGrid&lt;Tile&gt;, på sin side, utvider `Iterable<CoordinateItem<Tile>>`. Ergo har et `TetrisBoard` -objekt typen `Iterable<CoordinateItem<Tile>>`.

- Når hjørnene skal fargelegges, sier guiden at vi fargelegger hjørnene i konstruktøren til `TetrisModel`, etter at feltvariabelen med TetrisBoard blir initiert. For eksempel vil dette sette fargen til hjørnet oppe til venstre til grønn:
```java
TetrisModel( ... ) {
    this.tetrisBoard = new TetrisBoard( ... );
    this.tetrisBoard.set(new Coordinate(0, 0), new Tile('g', Color.GREEN));
}
```

- Når brettet skal tegnes, gjelder det å vite hvilken farge som skal tegnes hvor på brettet. Dette er tilgjengelig i TetrisView via metoden som returnerer en `Iterable<CoordinateItem<Tile>>`. Dersom metoden som returnerer en slik er kalt *tilesInBoard*, kan vi se igjennom alle flisene på brettet slik:

```java
for (CoordinateItem<Tile> coordinateItem : this.tetrisViewableObjektSomRepresentererModellenSomSkalTegnes.tilesInBoard()) {
    int row = coordinateItem.coordinate.row;
    int col = coordinateItem.coordinate.col;
    Tile tile = coordinateItem.item;
    ...
}
```

### Steg 3 (tegne brikken)

- For å opprette static final PieceShape S, må vi kalle på konstruktøren for å opprette et objekt. For eksempel:
```java
static final PieceShape S = new PieceShape(
    new Tile('s', Color.GREEN),
    new boolean[][] {
        { false, true, true },
        { true, true, false }
    }
);
```

- Når vi skal implementere iteratoren til PositionedPiece, kan vi benytte samme generelle idé som vi gjorde i iteratoren til Grid: opprett en liste `a`, legg til CoordinateItem&lt;Tile&gt; for de riktige posisjonene i listen, og så returnere `a.iterator()`.
    - For å legge til de riktige posisjonene i listen, benytt en nøstet for-løkke som itererer igjennom posisjonene inni selve fasongen (returverdien til getShape -metoden til PieceShape -objektet). Dersom verdien er `true`, opprett et Coordinate hvor raden er summen av den raden du itererer over i selve fasongen + raden som brikken har som helhet. Tilsvarende for kolonnen: den er summen av kolonnen vi er i innad i selve fasongen + kolonnen til brikken som helhet.

- For å tegne brikken: tilgjengeliggjør flisene til den fallende brikken *på akkurat samme måte* som vi tilgjengeliggjorde flisene til brettet. Det vil si, opprett en ny metode i TetrisViewable som returnerer `Iterable<CoordinateItem<Tile>>`. I TetrisView kan denne tegnes nøyaktig på samme måte som brettet, men ved å iterere over flisene man får fra denne metoden i stedet for flisene man får som kommer fra brettet.


### Steg 5 (rotasjon)

- Det finnes ulike standarder for hvordan brikken skal rotere, og avhengig av hvordan man implementerer `PieceShape`, kan noen former for rotasjon være enklere å implementere. Dersom `boolean[][]` som representerer fasongen i PieceShape alltid er kvadratisk, vil det ikke være nødvendig å flytte brikken tilbake til "sentrum" i rotasjonssteget. Ulempen er at vi må gjøre litt mer arbeid for å sikre at brikken plasseres øverst på brettet når den blir laget. Begge varianter vil bli godtatt, så lenge rotasjonen fremstår som pen (rundt ca midten av seg selv) og uten å drifte etter fire rotasjoner.

- For å få rotasjonen til å være *rundt sentrum* av brikken, finnes det flere metoder å gjøre det på. En relativt enkel metode går ut på følgende:
    - I PositionedPiece lar vi feltvariabelen som holder posisjonen representere den midterste flisen i brikken, og *ikke* hjørnet oppe til venstre. Dette betyr at *iterator* -metoden til PositionedPiece endres: der koordinatene som itereres gjennom tidligere var summen av posisjonen internt i fasongen og posisjonen til brikken som helhet, trekker vi nå *i tillegg* fra halvparten av brikkens høyde/bredde når vi regner ut rad/kolonne.
    - I PositionedPieceFactory vil brikkenes initielle posisjon regnes ut på en annen måte. Brikkens kolonne blir enklere, ved at det kun brukes samme verdi som man fikk da setCenterColumn ble kalt. Brikkens rad blir nå halvparten av brikkens høyde i stedet for 0.
    - Det vil nå ikke være behov for å reposisjonere brikken i det hele tatt under rotasjon.


# Errata

Her samler vi feil i guiden som vi oppdager underveis.

### Steg 2 (tegne brettet)

- Det er en logisk feil i `drawChess` -metoden i `SampleView` som ikke lar seg oppdage så lett når det tilfeldigvis er like mange rader som kolonner, men som kommer til uttrykk dersom man kopierer idéene til `drawBoardWithRightBottomPadding` i `TetrisView`. På linje 112 står det `int tileX = x + row * width / 8;`, men her skulle det stått `int tileX = x + col * width / 8;`. På samme måte bør `row` og `col` byttes om på de neste tre linjene også. Prinsipp: alle regnestykker som omhandler oppover/nedover bruker {y, row, height}, mens regnestykker som omhandler høyre/venstre bruker begrepene {x, col, width}.

### Steg 3 (tegne brikken)

- Bildet som viser koordinatene har en trykkfeil på selve bildet: koordinatene skal være (2, 1), (3, 1), (3, 2) og (3, 3) (som oppgitt i teksten). På bildet er koordinatet (3, 2) oppgitt to ganger, den lengst til venstre av dem skal egentlig være (3, 1).
- På slutten av avsnittet om PieceShape er det gjort en referanse til en klasse `Piece`. Det som menes er `PieceShape`.

### Steg 5 (rotasjon)

- Her gjøres en referanse til en klasse `Piece`. Det som egentlig menes er `PieceShape`.
